import React from "react";
import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import EventList from "./event/EventList";
import EventCreate from "./event/EventCreate";
import NavBar from "./NavBar";
import Message from "./Message";
class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <BrowserRouter>
        <NavBar />
        <Switch>
          <Route path="/" exact>
            <div>
              <div className="row">
                <EventList className="col-sm-6" name="Rahil" />
              </div>
            </div>
          </Route>
          <Route path="/message" exact>
            <div>
              <div className="row">
                <Message className="col-sm-6" name="Rahil" />
              </div>
            </div>
          </Route>
          <Route path="/add-event">
            <EventCreate className="col-sm-6" />
          </Route>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
