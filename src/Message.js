import React from "react";

class Message extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      message: "Hello Rahil Memon",
      seconds: 0
    };
  }

  componentDidMount() {
    setInterval(() => {
      this.setState({
        seconds: this.state.seconds + 1
      });
    }, 1000);
  }

  render() {
    return (
      <div className={this.props.className}>
        <h1>
          Helllllll {this.state.message} {this.state.seconds}{" "}
        </h1>
      </div>
    );
  }
}
export default Message;
