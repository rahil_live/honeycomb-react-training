import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { createStore, applyMiddleware, compose } from "redux";
import reducer from "./reducer";
import { Provider } from "react-redux";
import createSagaMiddleware from "redux-saga";
import eventSaga from "./event/saga";
let sagaMiddleWare = createSagaMiddleware();

const composeEnhancers =
  typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;
const enhancer = composeEnhancers(applyMiddleware(sagaMiddleWare));

const store = createStore(reducer, enhancer);

sagaMiddleWare.run(eventSaga);

ReactDOM.render(
  <Provider store={store}>
    <App name="Rahil" />
  </Provider>,
  document.getElementById("root")
);
