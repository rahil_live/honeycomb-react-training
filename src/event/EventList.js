import React, { Component } from "react";
import { connect } from "react-redux";

class EventList extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.props.dispatch({ type: "GET_EVENTS_REQUEST" });
  }

  handleSubmit(event) {
    event.preventDefault();

    let newEventObj = {
      id: this.props.events.length + 1,
      title: this.props.textTitle
    };

    this.props.dispatch({
      type: "ADD_EVENT",
      event: newEventObj
    });
  }

  handleChange(event) {
    this.props.dispatch({ type: "CHANGE_TEXT", name: event.target.value });
  }

  render() {
    return (
      <div className={this.props.className}>
        <h1> Events For {this.props.name} </h1>

        {this.props.requesting && <h1>Loading.....</h1>}
        {this.props.events &&
          this.props.events.map(event => {
            return (
              <div key={event.id}>
                <div className="card text-center">
                  <div className="card-header">{event.title}</div>
                  <div className="card-body">
                    <h5 className="card-title">{event.description}</h5>
                    <p className="card-text">{event.location}</p>
                  </div>
                  <div className="card-footer text-muted">
                    {" "}
                    {event.oraganiser}{" "}
                  </div>
                </div>
                <br />
              </div>
            );
          })}

        {/* <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Title</label>
            <input
              type="text"
              className="form-control"
              placeholder="Enter Title"
              onChange={this.handleChange}
              value={this.props.textTitle}
            />
            <small id="emailHelp" className="form-text text-muted">
              {this.props.textTitle}
            </small>
          </div>
          <button type="submit" className="btn btn-primary">
            {" "}
            ADD Event{" "}
          </button>
        </form> */}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    events: state.event.events,
    textTitle: state.event.text,
    requesting: state.event.requesting
  };
}

export default connect(mapStateToProps)(EventList);
