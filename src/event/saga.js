import { takeLatest, call, put } from "@redux-saga/core/effects";

import { get, post } from "../request";

const url = "http://localhost:3000/events";

function* addEvent(action) {
  let response = yield call(post, url, action.event);
  yield put({ type: "EVENT_ADD_DONE", event: response.data });
  action.cb();
}

function* getEvent() {
  let response = yield call(get, url);
  yield put({ type: "GET_EVENTS_DONE", events: response.data });
}
export default function* mySaga() {
  yield takeLatest("ADD_EVENT_REQUEST", addEvent);
  yield takeLatest("GET_EVENTS_REQUEST", getEvent);
}
