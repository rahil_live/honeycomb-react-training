import React, { Component } from "react";
import { connect } from "react-redux";

import { withRouter } from "react-router-dom";

class EventCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      description: "",
      oraganiser: "",
      location: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();

    if (!this.state.title) {
      alert("Please ADD title");
    } else {
      let newEventObj = {
        ...this.state
      };

      this.props.dispatch({
        type: "ADD_EVENT_REQUEST",
        event: newEventObj,
        cb: () => {
          this.props.history.push("/");
        }
      });
    }
  }
  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  render() {
    return (
      <div className={this.props.className}>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Title</label>
            <input
              type="text"
              name="title"
              className="form-control"
              placeholder="Enter Title"
              onChange={this.handleChange}
            />
            <small id="emailHelp" className="form-text text-muted">
              {this.props.textTitle}
            </small>
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Description</label>
            <input
              type="text"
              name="description"
              className="form-control"
              placeholder="Enter Description"
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Location</label>
            <input
              type="text"
              name="location"
              className="form-control"
              placeholder="Enter Location"
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Oraganiser</label>
            <input
              type="text"
              name="oraganiser"
              className="form-control"
              placeholder="Enter Oraganiser"
              onChange={this.handleChange}
            />
          </div>
          <button type="submit" className="btn btn-primary">
            ADD Event
          </button>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    events: state.events
  };
}

export default connect(mapStateToProps)(withRouter(EventCreate));
