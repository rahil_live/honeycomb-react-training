let initialState = {
  text: "",
  events: [],
  requesting: false
};

let reducer = (state = initialState, action) => {
  switch (action.type) {
    case "CHANGE_TEXT":
      return {
        ...state,
        text: action.name
      };
    case "GET_EVENTS_REQUEST":
      return { ...state, requesting: true };
    case "GET_EVENTS_DONE":
      return {
        ...state,
        events: action.events,
        requesting: false
      };
  }
  return state;
};

export default reducer;
